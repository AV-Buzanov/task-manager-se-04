package ru.buzanov.tm.bootstrap;

import ru.buzanov.tm.constant.CommandConst;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;
import ru.buzanov.tm.service.ProjectService;
import ru.buzanov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Bootstrap {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private TaskService taskService = new TaskService(projectRepository, taskRepository);
    private SimpleDateFormat dateFormat = new SimpleDateFormat(FormatConst.DATE_FORMAT, Locale.ENGLISH);

    public void start() {
        boolean out = true;
        String buffer = "";

        System.out.println("***WELCOME TO TASK MANAGER***");

        while (out) {

            try {
                buffer = reader.readLine();

                switch (buffer) {
                    case (CommandConst.CREATE_PROJECT):
                        createProject();
                        break;

                    case (CommandConst.CLEAR_PROJECT):
                        clearProject();
                        break;

                    case (CommandConst.LIST_PROJECT):
                        listProject();
                        break;

                    case (CommandConst.EDIT_PROJECT):
                        editProject();
                        break;

                    case (CommandConst.REMOVE_PROJECT):
                        removeProject();
                        break;

                    case (CommandConst.VIEW_PROJECT):
                        viewProject();
                        break;

                    case (CommandConst.CREATE_TASK):
                        createTask();
                        break;

                    case (CommandConst.CLEAR_TASK):
                        clearTask();
                        break;

                    case (CommandConst.LIST_TASK):
                        listTask();
                        break;

                    case (CommandConst.EDIT_TASK):
                        editTask();
                        break;

                    case (CommandConst.VIEW_TASK):
                        viewTask();
                        break;

                    case (CommandConst.REMOVE_TASK):
                        removeTask();
                        break;

                    case (CommandConst.HELP):
                        helpOut();
                        break;

                    case (CommandConst.EXIT):
                        out = false;
                        break;
                }
            } catch (ParseException e) {
                System.out.println("Wrong input, try again");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Selected index doesn't exist");
            } catch (Exception e) {
                System.out.println("Something wrong, try again");
            }
        }
    }

    private void createProject() throws Exception {
        Project project = new Project();
        String stringBuf;
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME]");
        stringBuf = reader.readLine();
        if ("".equals(stringBuf)) {
            System.out.println("Project name can't be empty");
            return;
        }
        if (projectService.isNameExist(stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        project.setName(stringBuf);
        System.out.println("[ENTER PROJECT START DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER PROJECT END DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setDescription(stringBuf);
        stringBuf = projectService.persist().getId();
        projectService.merge(stringBuf, project);
        System.out.println("[OK]");
    }

    private void listProject() {
        System.out.println("[PROJECT LIST]");
        System.out.println(projectService.getList());
    }

    private void viewProject() throws Exception {
        System.out.println("[CHOOSE PROJECT TO VIEW]");
        System.out.println(projectService.getList());
        String idBuf = projectService.getIdByCount(Integer.parseInt(reader.readLine()));
        Project projectBuf = projectService.findOne(idBuf);
        System.out.println("[NAME] ");
        System.out.println(projectBuf.getName());
        System.out.println("[START DATE] ");
        if (projectBuf.getStartDate() != null)
            System.out.println(dateFormat.format(projectBuf.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[END DATE] ");
        if (projectBuf.getEndDate() != null)
            System.out.println(dateFormat.format(projectBuf.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[DESCRIPTION] ");
        if (projectBuf.getDescription() != null)
            System.out.println(projectBuf.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[TASKS]");
        if (taskService.findByProjectId(idBuf).isEmpty()) {
            System.out.println(FormatConst.EMPTY_FIELD);
            return;
        }
        for (Task task : taskService.findByProjectId(idBuf)) {
            System.out.println(task.getName());
        }
    }

    private void editProject() throws Exception {
        System.out.println("[CHOOSE PROJECT TO EDIT]");
        System.out.println(projectService.getList());
        Project project = projectService.findOne(projectService.getIdByCount(Integer.parseInt(reader.readLine())));
        System.out.println("[NAME]");
        System.out.println(project.getName());
        System.out.println("[ENTER NEW NAME]");
        String stringBuf = reader.readLine();
        if (projectService.isNameExist(stringBuf)) {
            System.out.println("Project with this name already exist.");
            return;
        }
        if (!"".equals(stringBuf))
            project.setName(stringBuf);

        System.out.println("[START DATE]");
        if (project.getStartDate() != null)
            System.out.println(dateFormat.format(project.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[END DATE]");
        if (project.getEndDate() != null)
            System.out.println(dateFormat.format(project.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[DESCRIPTION]");
        if (project.getDescription() != null)
            System.out.println(project.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            project.setDescription(stringBuf);
        projectService.merge(project.getId(), project);
        System.out.println("[OK]");
    }

    private void removeProject() throws Exception {
        System.out.println("[CHOOSE PROJECT TO REMOVE]");
        System.out.println(projectService.getList());
        String idBuf = projectService.getIdByCount(Integer.parseInt(reader.readLine()));
        projectService.remove(idBuf);
        taskService.removeByProjectId(idBuf);
        System.out.println("[OK]");
    }

    private void clearProject() {
        for (Project project : projectService.findAll())
            taskService.removeByProjectId(project.getId());
        projectService.removeAll();
        System.out.println("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    private void createTask() throws Exception {
        Task task = new Task();
        String stringBuf;
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME]");
        stringBuf = reader.readLine();
        if ("".equals(stringBuf)) {
            System.out.println("Task name can't be empty");
            return;
        }
        if (taskService.isNameExist(stringBuf)) {
            System.out.println("Task with this name already exist.");
            return;
        }
        task.setName(stringBuf);
        System.out.println("[ENTER TASK START DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER TASK END DATE " + FormatConst.DATE_FORMAT + " ]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setDescription(stringBuf);
        if (!projectService.findAll().isEmpty()) {
            System.out.println("[CHOOSE PROJECT]");
            System.out.println(projectService.getList());
            stringBuf = reader.readLine();
            if (!"".equals(stringBuf)) {
                task.setProjectId(projectService.getIdByCount(Integer.parseInt(stringBuf)));
            }
        }
        stringBuf = taskService.persist().getId();
        taskService.merge(stringBuf, task);
        System.out.println("[OK]");
    }

    private void listTask() {
        System.out.println("[TASK LIST]");
        System.out.println(taskService.getList());
    }

    private void viewTask() throws Exception {
        System.out.println("[CHOOSE TASK TO VIEW]");
        System.out.println(taskService.getList());
        String idBuf = taskService.getIdByCount(Integer.parseInt(reader.readLine()));
        Task task = taskService.findOne(idBuf);
        System.out.println("[NAME]");
        System.out.println(task.getName());
        System.out.println("[START DATE]");
        if (task.getStartDate() != null)
            System.out.println(dateFormat.format(task.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[END DATE]");
        if (task.getEndDate() != null)
            System.out.println(dateFormat.format(task.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[DESCRIPTION]");
        if (task.getDescription() != null)
            System.out.println(task.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[PROJECT]");
        if (task.getProjectId() != null)
            System.out.println(projectService.findOne(task.getProjectId()).getName());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
    }

    private void editTask() throws Exception {
        String stringBuf;
        System.out.println("[CHOOSE TASK TO EDIT]");
        System.out.println(taskService.getList());
        stringBuf = taskService.getIdByCount(Integer.parseInt(reader.readLine()));
        Task task = taskService.findOne(stringBuf);
        System.out.println("[NAME]");
        System.out.println(task.getName());
        System.out.println("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (taskService.isNameExist(stringBuf)) {
            System.out.println("Task with this name already exist.");
            return;
        }
        if (!"".equals(stringBuf))
            task.setName(stringBuf);
        System.out.println("[START DATE]");
        if (task.getStartDate() != null)
            System.out.println(dateFormat.format(task.getStartDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setStartDate(dateFormat.parse(stringBuf));
        System.out.println("[END DATE]");
        if (task.getEndDate() != null)
            System.out.println(dateFormat.format(task.getEndDate()));
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setEndDate(dateFormat.parse(stringBuf));
        System.out.println("[DESCRIPTION]");
        if (task.getDescription() != null)
            System.out.println(task.getDescription());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf))
            task.setDescription(stringBuf);
        System.out.println("[PROJECT]");
        if (task.getProjectId() != null)
            System.out.println(projectService.findOne(task.getProjectId()).getName());
        else
            System.out.println(FormatConst.EMPTY_FIELD);
        System.out.println("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        System.out.println(projectService.getList());
        stringBuf = reader.readLine();
        if (!"".equals(stringBuf)) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = projectService.getIdByCount(Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        taskService.merge(task.getId(), task);
        System.out.println("[OK]");
    }

    private void removeTask() throws Exception {
        System.out.println("[CHOOSE TASK TO REMOVE]");
        System.out.println(taskService.getList());
        String idBuf = taskService.getIdByCount(Integer.parseInt(reader.readLine()));
        taskService.remove(idBuf);
        System.out.println("[OK]");
    }

    private void clearTask() {
        taskService.removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }

    private void helpOut() {
        System.out.println(CommandConst.HELP + ": Show all commands.");
        System.out.println(CommandConst.CREATE_PROJECT + ": Create new project.");
        System.out.println(CommandConst.LIST_PROJECT + ": Show all projects.");
        System.out.println(CommandConst.VIEW_PROJECT + ": View project information");
        System.out.println(CommandConst.EDIT_PROJECT + ": Edit project");
        System.out.println(CommandConst.REMOVE_PROJECT + ": Remove selected project with connected tasks.");
        System.out.println(CommandConst.CLEAR_PROJECT + ": Remove all projects.");

        System.out.println(CommandConst.CREATE_TASK + ": Create new task.");
        System.out.println(CommandConst.LIST_TASK + ": Show all tasks.");
        System.out.println(CommandConst.VIEW_TASK + ": View task information");
        System.out.println(CommandConst.EDIT_TASK + ": Edit task");
        System.out.println(CommandConst.REMOVE_TASK + ": Remove selected tasks.");
        System.out.println(CommandConst.CLEAR_TASK + ": Remove all tasks.");

        System.out.println(CommandConst.EXIT + ": Close program");
    }
}