package ru.buzanov.tm.service;

import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TaskService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public TaskService() {
    }

    public TaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task persist() {
        return taskRepository.persist();
    }

    public boolean isNameExist(String name) {
        for (Task task : findAll()) {
            if (task.getName().equals(name))
                return true;
        }
        return false;
    }

    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String id) {
        return taskRepository.findOne(id);
    }

    public Collection<Task> findByProjectId(String projectId) {
        Collection<Task> tasks = new ArrayList<>();
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                tasks.add(task);
        }
        return tasks;
    }

    public void merge(String id, Task task) {
        taskRepository.merge(id, task);
    }

    public Task remove(String id) {
        return taskRepository.remove(id);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeByProjectId(String projectId) {
        Iterator it = findAll().iterator();
        while (it.hasNext()) {
            Task task = (Task) it.next();
            if (projectId.equals(task.getProjectId()))
                it.remove();
        }
    }

    public String getList() {
        int indexBuf = 1;
        String stringBuf = "";
        for (Task task : findAll()) {
            stringBuf = stringBuf.concat(Integer.toString(indexBuf));
            stringBuf = stringBuf.concat(". " + task.getName());
            if (findAll().size() > indexBuf)
                stringBuf = stringBuf.concat(System.getProperty("line.separator"));
            indexBuf++;
        }
        return stringBuf;
    }

    public String getIdByCount(int count) {
        int indexBuf = 1;
        for (Task task : findAll()) {
            if (indexBuf == count)
                return task.getId();
            indexBuf++;
        }
        return null;
    }

}