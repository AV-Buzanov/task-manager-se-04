package ru.buzanov.tm.service;

import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.repository.ProjectRepository;
import ru.buzanov.tm.repository.TaskRepository;

import java.util.Collection;

public class ProjectService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    public ProjectService() {
    }

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }


    public Project persist() {
        return projectRepository.persist();
    }

    public boolean isNameExist(String name) {
        for (Project project : findAll()) {
            if (project.getName().equals(name))
                return true;
        }
        return false;
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String id) {
        return projectRepository.findOne(id);
    }

    public void merge(String id, Project project) {
        projectRepository.merge(id, project);
    }

    public Project remove(String id) {
        return projectRepository.remove(id);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public String getList() {
        int indexBuf = 1;
        String stringBuf = "";
        for (Project project : findAll()) {
            stringBuf = stringBuf.concat(Integer.toString(indexBuf));
            stringBuf = stringBuf.concat(". " + project.getName());
            if (findAll().size() > indexBuf)
                stringBuf = stringBuf.concat(System.getProperty("line.separator"));
            indexBuf++;
        }
        return stringBuf;
    }

    public String getIdByCount(int count) {
        int indexBuf = 1;
        for (Project project : findAll()) {
            if (indexBuf == count)
                return project.getId();
            indexBuf++;
        }
        return null;
    }
}