package ru.buzanov.tm.constant;

public class CommandConst {
    public final static String CREATE_PROJECT = "project-create";
    public final static String LIST_PROJECT = "project-list";
    public final static String VIEW_PROJECT = "project-view";
    public final static String EDIT_PROJECT = "project-edit";
    public final static String REMOVE_PROJECT = "project-remove";
    public final static String CLEAR_PROJECT = "project-clear";

    public final static String CREATE_TASK = "task-create";
    public final static String LIST_TASK = "task-list";
    public final static String VIEW_TASK = "task-view";
    public final static String EDIT_TASK = "task-edit";
    public final static String REMOVE_TASK = "task-remove";
    public final static String CLEAR_TASK = "task-clear";

    public final static String EXIT = "exit";
    public final static String HELP = "help";
}