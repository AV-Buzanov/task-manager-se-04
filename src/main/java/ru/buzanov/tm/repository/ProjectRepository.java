package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {
    private Map<String, Project> projects = new LinkedHashMap<>();

    public Project persist() {
        Project project = new Project();
        projects.put(project.getId(), project);
        return project;
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findOne(String id) {
        if (projects.containsKey(id))
            return projects.get(id);
        else return null;
    }

    public void merge(String id, Project project) {
        if (project.getName() != null)
            projects.get(id).setName(project.getName());
        if (project.getStartDate() != null)
            projects.get(id).setStartDate(project.getStartDate());
        if (project.getEndDate() != null)
            projects.get(id).setEndDate(project.getEndDate());
        if (project.getDescription() != null)
            projects.get(id).setDescription(project.getDescription());
    }

    public Project remove(String id) {
        return projects.remove(id);
    }

    public void removeAll() {
        projects.clear();
    }
}