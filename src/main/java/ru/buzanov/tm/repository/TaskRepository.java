package ru.buzanov.tm.repository;

import ru.buzanov.tm.entity.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;


public class TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public Task persist() {
        Task task = new Task();
        tasks.put(task.getId(), task);
        return task;
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findOne(String id) {
        if (tasks.containsKey(id))
            return tasks.get(id);
        else return null;
    }

    public void merge(String id, Task task) {
        if (task.getName() != null)
            tasks.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            tasks.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            tasks.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            tasks.get(id).setDescription(task.getDescription());
        if (task.getProjectId() != null)
            tasks.get(id).setProjectId(task.getProjectId());
    }

    public Task remove(String id) {
        return tasks.remove(id);
    }

    public void removeAll() {
        tasks.clear();
    }
}
